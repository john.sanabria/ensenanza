from PIL import Image, ImageFilter
import numpy as np
import os

def cargar_imagen(ruta):
    """Carga una imagen desde la ruta especificada y la convierte a escala de grises."""
    return Image.open(ruta).convert("L")

def aplicar_filtro(imagen,ruta):
    """Aplica un filtro de desenfoque a la imagen."""
    filtered_image = imagen.filter(ImageFilter.BLUR)
    dissected_ruta = os.path.splitext(ruta)
    filtered_image.save(f"{dissected_ruta[0]}-blurred{dissected_ruta[1]}")
    return imagen.filter(ImageFilter.BLUR)

def transformar_imagen(imagen):
    """Escala la imagen a la mitad de su tamaño original."""
    width, height = imagen.size
    return imagen.resize((width // 2, height // 2))

def calcular_brillo_promedio(imagen):
    """Calcula el brillo promedio de la imagen."""
    np_imagen = np.array(imagen)
    return np.mean(np_imagen)

def guardar_imagen(imagen, ruta):
    """Guarda la imagen en la ruta especificada."""
    imagen.save(ruta)

def procesamiento_imagen(ruta_entrada, ruta_salida):
    """Realiza el procesamiento secuencial de la imagen."""
    # Cargar la imagen
    imagen = cargar_imagen(ruta_entrada)
    
    # Aplicar filtro de desenfoque
    imagen_filtrada = aplicar_filtro(imagen,ruta_salida)
    
    # Transformar la imagen (escalar)
    imagen_transformada = transformar_imagen(imagen_filtrada)
    
    # Calcular el brillo promedio
    brillo_promedio = calcular_brillo_promedio(imagen_transformada)
    print(f"Brillo promedio: {brillo_promedio}")
    
    # Guardar la imagen procesada
    guardar_imagen(imagen_transformada, ruta_salida)

# Uso del código
procesamiento_imagen("entrada.png", "salida.png")
