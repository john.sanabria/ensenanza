from PIL import Image, ImageFilter
import numpy as np
from concurrent.futures import ThreadPoolExecutor

def cargar_imagen(ruta):
    """Carga una imagen desde la ruta especificada y la convierte a escala de grises."""
    return Image.open(ruta).convert("L")

def aplicar_filtro_y_transformar(parte_imagen):
    """Aplica un filtro de desenfoque y escala la imagen a la mitad de su tamaño original."""
    imagen_filtrada = parte_imagen.filter(ImageFilter.BLUR)
    width, height = imagen_filtrada.size
    imagen_transformada = imagen_filtrada.resize((width // 2, height // 2))
    return imagen_transformada

def calcular_brillo_promedio(imagen):
    """Calcula el brillo promedio de la imagen."""
    np_imagen = np.array(imagen)
    return np.mean(np_imagen)

def dividir_imagen(imagen, num_partes):
    """Divide la imagen en num_partes partes."""
    width, height = imagen.size
    step = width // num_partes
    return [imagen.crop((i * step, 0, (i + 1) * step, height)) for i in range(num_partes)]

def combinar_imagenes(imagenes_parciales):
    """Combina partes de la imagen en una sola imagen."""
    widths, heights = zip(*(i.size for i in imagenes_parciales))
    total_width = sum(widths)
    max_height = max(heights)
    imagen_final = Image.new("L", (total_width, max_height))
    x_offset = 0
    for im in imagenes_parciales:
        imagen_final.paste(im, (x_offset, 0))
        x_offset += im.size[0]
    return imagen_final

def procesamiento_imagen_map_reduce(ruta_entrada, ruta_salida, num_partes=4):
    """Realiza el procesamiento de la imagen usando Map-Reduce."""
    imagen = cargar_imagen(ruta_entrada)
    
    # MAP: Dividir la imagen en partes y procesarlas en paralelo
    partes_imagen = dividir_imagen(imagen, num_partes)
    
    with ThreadPoolExecutor() as executor:
        partes_transformadas = list(executor.map(aplicar_filtro_y_transformar, partes_imagen))
    
    # REDUCE: Calcular el brillo promedio de cada parte en paralelo
    with ThreadPoolExecutor() as executor:
        brillos_promedio = list(executor.map(calcular_brillo_promedio, partes_transformadas))
    
    brillo_promedio_total = np.mean(brillos_promedio)
    print(f"Brillo promedio total: {brillo_promedio_total}")
    
    # Combinar las partes procesadas en una sola imagen
    imagen_final = combinar_imagenes(partes_transformadas)
    
    # Guardar la imagen final procesada
    guardar_imagen(imagen_final, ruta_salida)

def guardar_imagen(imagen, ruta):
    """Guarda la imagen en la ruta especificada."""
    imagen.save(ruta)

# Uso del código
procesamiento_imagen_map_reduce("entrada.png", "salida_map_reduce.png")

