from PIL import Image, ImageFilter
import numpy as np
from queue import Queue
from threading import Thread

def cargar_imagen(ruta):
    """Carga una imagen desde la ruta especificada y la convierte a escala de grises."""
    return Image.open(ruta).convert("L")

def productor(cola, partes_imagen):
    """Función del productor: coloca partes de la imagen en la cola."""
    for parte_imagen in partes_imagen:
        cola.put(parte_imagen)
    # Coloca un sentinela (None) para indicar el fin de la producción
    for _ in range(num_consumidores):
        cola.put(None)

def consumidor(cola, salida, brillos):
    """Función del consumidor: procesa las partes de la imagen de la cola."""
    while True:
        parte_imagen = cola.get()
        if parte_imagen is None:
            break
        # Procesar la parte de la imagen
        imagen_filtrada = parte_imagen[0].filter(ImageFilter.BLUR)
        width, height = imagen_filtrada.size
        imagen_transformada = imagen_filtrada.resize((width // 2, height // 2))
        brillo_promedio = np.mean(np.array(imagen_transformada))
        salida.put((imagen_transformada, parte_imagen[1]))
        brillos.append(brillo_promedio)
        cola.task_done()

def combinar_imagenes(imagenes_parciales):
    """Combina partes de la imagen en una sola imagen."""
    # Filtrar None y ordenar por el índice de la parte
    imagenes_parciales = [i for i in imagenes_parciales if i[0] is not None]
    imagenes_parciales.sort(key=lambda x: x[1])
    imagenes = [i[0] for i in imagenes_parciales]
    widths, heights = zip(*(i.size for i in imagenes))
    total_width = sum(widths)
    max_height = max(heights)
    imagen_final = Image.new("L", (total_width, max_height))
    x_offset = 0
    for im in imagenes:
        imagen_final.paste(im, (x_offset, 0))
        x_offset += im.size[0]
    return imagen_final

def procesamiento_imagen_producer_consumer(ruta_entrada, ruta_salida, num_partes=4):
    """Realiza el procesamiento de la imagen usando el patrón Producer-Consumer."""
    imagen = cargar_imagen(ruta_entrada)

    # Dividir la imagen en partes
    partes_imagen = [(parte, i) for i, parte in enumerate(dividir_imagen(imagen, num_partes))]

    cola = Queue()
    salida = Queue()
    brillos = []

    # Crear y lanzar hilos del productor
    productor_thread = Thread(target=productor, args=(cola, partes_imagen))
    productor_thread.start()

    # Crear y lanzar hilos del consumidor
    consumidores = []
    for _ in range(num_consumidores):
        t = Thread(target=consumidor, args=(cola, salida, brillos))
        t.start()
        consumidores.append(t)

    # Esperar a que el productor termine
    productor_thread.join()

    # Esperar a que todos los consumidores terminen
    for t in consumidores:
        t.join()

    # Calcular el brillo promedio total después de que todos los consumidores hayan terminado
    brillo_promedio_total = np.mean(brillos)
    print(f"Brillo promedio total: {brillo_promedio_total}")

    # Recoger las imágenes procesadas
    partes_transformadas = []
    while not salida.empty():
        partes_transformadas.append(salida.get())

    # Combinar las partes procesadas en una sola imagen
    imagen_final = combinar_imagenes(partes_transformadas)

    # Guardar la imagen final procesada
    guardar_imagen(imagen_final, ruta_salida)

def dividir_imagen(imagen, num_partes):
    """Divide la imagen en num_partes partes."""
    width, height = imagen.size
    step = width // num_partes
    return [imagen.crop((i * step, 0, (i + 1) * step, height)) for i in range(num_partes)]

def guardar_imagen(imagen, ruta):
    """Guarda la imagen en la ruta especificada."""
    imagen.save(ruta)

# Configuración
num_consumidores = 4

# Uso del código
procesamiento_imagen_producer_consumer("entrada.png", "salida_producer_consumer.png")

