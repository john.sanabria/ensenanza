from PIL import Image, ImageFilter
import numpy as np
from concurrent.futures import ThreadPoolExecutor

def cargar_imagen(ruta):
    """Carga una imagen desde la ruta especificada y la convierte a escala de grises."""
    # Esta conversión a tono de grises se hace para facilitar la conversión
    # más adelante de la imagen a un arreglo de numpy y calcular la media de
    # la imagen.
    return Image.open(ruta).convert("L")

def aplicar_filtro(parte_imagen):
    """Aplica un filtro de desenfoque a una parte de la imagen."""
    return parte_imagen.filter(ImageFilter.BLUR)

def transformar_imagen(parte_imagen):
    """Escala una parte de la imagen a la mitad de su tamaño original."""
    width, height = parte_imagen.size
    return parte_imagen.resize((width // 2, height // 2))

def calcular_brillo_promedio(parte_imagen):
    """Calcula el brillo promedio de una parte de la imagen."""
    np_imagen = np.array(parte_imagen)
    return np.mean(np_imagen)

def guardar_imagen(imagen, ruta):
    """Guarda la imagen en la ruta especificada."""
    imagen.save(ruta)

def dividir_imagen(imagen, num_partes):
    """Divide la imagen en num_partes partes."""
    width, height = imagen.size
    step = width // num_partes
    # La función 'crop' toma cuatro argumentos, los dos primeros representan
    # la posición (x1,y1) y los dos últimos la posición (x2,y2). 
    #
    # (x1,y1)
    #    +---------------------------------+
    #    |                                 |
    #    |                                 |
    #    |                                 |
    #    |                                 |
    #    |                                 |
    #    |                                 |
    #    |                                 |
    #    |                                 |
    #    |                                 |
    #    +---------------------------------+
    #                                   (x2,y2)
    #
    return [imagen.crop((i * step, 0, (i + 1) * step, height)) for i in range(num_partes)]

def combinar_imagenes(imagenes_parciales):
    """Combina partes de la imagen en una sola imagen."""
    # La instrucción zip(*(i.size for i in imagenes_parciales)) devuelve dos
    # listas de números. 'i.size' es una tupla que tiene (width, height). 
    # Entonces se crea un arreglo con los diferentes 'sizes' de las imagenes.
    # '*' lo que hace es 'desbaratar' el arreglo y pasarle a la función 'zip'
    # una serie de tuplas (width1, height1), (width2, height2), ... y entonces
    # 'zip' deja en 'widths' = (width1, width2,...) y en 
    # 'heights' = (height1, height2,...)
    #
    widths, heights = zip(*(i.size for i in imagenes_parciales))
    total_width = sum(widths)
    max_height = max(heights)
    imagen_final = Image.new("L", (total_width, max_height))
    x_offset = 0
    #
    # En este ciclo se pegan las imagenes
    #
    for im in imagenes_parciales:
        imagen_final.paste(im, (x_offset, 0))
        x_offset += im.size[0]
    return imagen_final

def procesamiento_imagen_paralelo(ruta_entrada, ruta_salida, num_partes=4):
    """Realiza el procesamiento paralelo de la imagen usando Fork-Join."""
    imagen = cargar_imagen(ruta_entrada)
    
    # Dividir la imagen en partes
    partes_imagen = dividir_imagen(imagen, num_partes)
    
    with ThreadPoolExecutor() as executor:
        # Aplicar filtro a cada parte en paralelo
        partes_filtradas = list(executor.map(aplicar_filtro, partes_imagen))
        
        # Transformar cada parte en paralelo
        partes_transformadas = list(executor.map(transformar_imagen, partes_filtradas))
        
        # Calcular brillo promedio de cada parte en paralelo
        brillos_promedio = list(executor.map(calcular_brillo_promedio, partes_transformadas))  
        brillo_total = np.mean(brillos_promedio)
        print(f"Brillo promedio total: {brillo_total}")
    
    # Combinar las partes transformadas en una sola imagen
    imagen_final = combinar_imagenes(partes_transformadas)
    
    # Guardar la imagen final procesada
    guardar_imagen(imagen_final, ruta_salida)

# Uso del código
procesamiento_imagen_paralelo("entrada.png", "salida_paralela.png")

