# Patrones de Software

En este directorio se encontrarán ejemplos de algunos patrones de software usados en programación en paralelo usando el lenguaje de programación Python.
Para mostrar algunos de estos patrones de software se usa una aplicación que hace un tratamiento básico de una imagen, se le aplica un filtro para difuminar la imagen y se le hace un escalamiento a la imagen (se decrementa su tamaño a la mitad del tamaño original).

- [00-sequential.py](./00-sequential.py) contiene la ejecución secuencial de los pasos para tratar la imagen como se describió en el párrafo anterior.

- [01-fork_join.py](./01-fork_join.py) ejemplifica el patrón **Fork-Join**.

- [02-03-pipeline_correcta.py](./02-03-pipeline_correcta.py) presenta el patrón **Pipeline**. Hay dos versiones anteriores de este código, una versión presenta errores al momento de ejecutar el código y otra versión no presenta errores en la ejecución pero arma la imagen de forma errónea.

- [03-mapreduce.py](./03-mapreduce.py) muestra el uso del patrón **MapReduce**.

- [04-producer_consumer.py](./04-producer_consumer) presenta el patrón **Producer-Consumer**.

## Preliminar de uso del software

Este código escrito en Python tiene algunas dependencias de librerías que se deben instalar. 
Para llevar a cabo este proceso se debe:

1- Crear un ambiente donde instalar las librerías, ejecutar `python3 -m venv venv`.

2- Activar el ambiente donde instalar las librerías, ejecutar `source venv/bin/activate`.

3- Instalar las librerias, ejecutar `pip install -r requirements.txt`.

---

Una vez instaladas las dependencias puede proceder a usar el software.
