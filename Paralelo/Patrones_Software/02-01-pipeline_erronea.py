from PIL import Image, ImageFilter
import numpy as np
from queue import Queue
from threading import Thread

def cargar_imagen(ruta):
    """Carga una imagen desde la ruta especificada y la convierte a escala de grises."""
    return Image.open(ruta).convert("L")

def aplicar_filtro(entrada_q, salida_q):
    """Aplica un filtro de desenfoque a la imagen recibida en la cola de entrada."""
    while True:
        parte_imagen = entrada_q.get()
        if parte_imagen is None:
            break
        imagen_filtrada = parte_imagen.filter(ImageFilter.BLUR)
        salida_q.put(imagen_filtrada)
        entrada_q.task_done()

def transformar_imagen(entrada_q, salida_q):
    """Escala la imagen a la mitad de su tamaño original."""
    while True:
        parte_imagen = entrada_q.get()
        if parte_imagen is None:
            break
        width, height = parte_imagen.size
        imagen_transformada = parte_imagen.resize((width // 2, height // 2))
        salida_q.put(imagen_transformada)
        entrada_q.task_done()

def calcular_brillo_promedio(entrada_q):
    """Calcula el brillo promedio de la imagen."""
    brillos = []
    while True:
        parte_imagen = entrada_q.get()
        if parte_imagen is None:
            break
        np_imagen = np.array(parte_imagen)
        brillo = np.mean(np_imagen)
        brillos.append(brillo)
        entrada_q.task_done()
    brillo_promedio_total = np.mean(brillos)
    print(f"Brillo promedio total: {brillo_promedio_total}")

def guardar_imagen(imagen, ruta):
    """Guarda la imagen en la ruta especificada."""
    imagen.save(ruta)

def dividir_imagen(imagen, num_partes):
    """Divide la imagen en num_partes partes."""
    width, height = imagen.size
    step = width // num_partes
    return [imagen.crop((i * step, 0, (i + 1) * step, height)) for i in range(num_partes)]

def combinar_imagenes(imagenes_parciales):
    """Combina partes de la imagen en una sola imagen."""
    widths, heights = zip(*(i.size for i in imagenes_parciales))
    total_width = sum(widths)
    max_height = max(heights)
    imagen_final = Image.new("L", (total_width, max_height))
    x_offset = 0
    for im in imagenes_parciales:
        imagen_final.paste(im, (x_offset, 0))
        x_offset += im.size[0]
    return imagen_final

def procesamiento_imagen_pipeline(ruta_entrada, ruta_salida, num_partes=4):
    """Realiza el procesamiento de la imagen usando un pipeline."""
    imagen = cargar_imagen(ruta_entrada)
    
    partes_imagen = dividir_imagen(imagen, num_partes)
    
    # Crear colas para conectar las etapas del pipeline
    cola_filtro = Queue()
    cola_transformacion = Queue()
    cola_brillo = Queue()

    # Crear y lanzar hilos para cada etapa del pipeline
    threads = []
    for _ in range(num_partes):
        t_filtro = Thread(target=aplicar_filtro, args=(cola_filtro, cola_transformacion))
        t_filtro.start()
        threads.append(t_filtro)
        
        t_transformacion = Thread(target=transformar_imagen, args=(cola_transformacion, cola_brillo))
        t_transformacion.start()
        threads.append(t_transformacion)
    
    # Lanzar hilo para calcular el brillo promedio
    t_brillo = Thread(target=calcular_brillo_promedio, args=(cola_brillo,))
    t_brillo.start()
    threads.append(t_brillo)
    
    # Colocar partes de la imagen en la primera cola para iniciar el pipeline
    for parte in partes_imagen:
        cola_filtro.put(parte)
    
    # Colocar sentinelas (None) para indicar el final de los datos
    for _ in range(num_partes):
        cola_filtro.put(None)
        cola_transformacion.put(None)
        cola_brillo.put(None)
    
    # Esperar a que todos los hilos terminen
    for t in threads:
        t.join()

    # Combinar las imágenes procesadas
    partes_transformadas = []
    while not cola_brillo.empty():
        partes_transformadas.append(cola_brillo.get())

    imagen_final = combinar_imagenes(partes_transformadas)

    # Guardar la imagen final procesada
    guardar_imagen(imagen_final, ruta_salida)

# Uso del código
procesamiento_imagen_pipeline("entrada.png", "salida_pipeline.png")

