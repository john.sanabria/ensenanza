# Codigos

En este directorio se encuentran algunos algoritmos o códigos que se pueden usar para llevar a cabo actividades de programación que le permitan a los estudiantes evidenciar el desarrollo de alguna competencia.

## Descripción de los directorios

- [procesamiento_basico_imagenes](./procesamiento_basico_imagenes) Aplicación escrita en C que permita la aplicación de algunos filtros básicos a una imagen PNG.
