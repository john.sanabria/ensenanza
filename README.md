# Enseñanza

Este repositorio contiene un conjunto de códigos y algoritmos que uso frecuentemente en mis cursos de `Sistemas Operativos` e `Infraestructuras Paralelas y Distribuias` los cuales dicto en la Universidad del Valle.

## Descripción de directorios

- [SistemasOperativos](./SistemasOperativos) códigos usados en el curso de Sistemas Operativos.

- [Paralelo](./Paralelo) códigos usados exclusivamente en el curso de `Infraestructuras Paralelas y Distribuidas` en la sección relacionada con programación en paralelo.

- [Codigos](./Codigos) tiene diferentes tipos de códigos que se pueden usar o para ejemplificar un concepto o como base para desarrollar actividades de programación que le permitan a los estudianes evidenciar algún tipo de competencia académica.

## Datos del autor

- Nombre: __John Sanabria__
- E-mail: __john.sanabria@correounivalle.edu.co__
- Universidad: __Universidad del Valle__
- Dependencia: __Escuela de Ingeniería de Sistemas y Computación__
