/*
 * Codigo tomado de: 
 * https://www.geeksforgeeks.org/dining-philosopher-problem-using-semaphores/
 *
 * Adaptado por: John Sanabria - john.sanabria@correounivalle.edu.co
 * Fecha: 2023-05-24
 */
#include <pthread.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>

#define N 5
#define THINKING 2
#define HUNGRY 1
#define EATING 0
#define LEFT (phnum + (N - 1)) % N
#define RIGHT (phnum + 1) % N
#define MAX_TIME 4
#define MIN_TIME 1
#define TIEMPO (rand() % (MAX_TIME - MIN_TIME + 1)) + MIN_TIME
#define eating(t) sleep(t)
#define thinking(t) sleep(t)

int state[N];
int phil[N];
int ate[N];

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t S[N];

void statistics() {
  int i;
  for (i = 0; i < N;i++) {
    printf("Filosofo %d comio %d veces\n", i + 1, ate[i]);
  }
}
void controlc() {
  statistics();
  exit(0);
}
void test(int phnum)
{
  if (state[phnum] == HUNGRY
      && state[LEFT] != EATING
      && state[RIGHT] != EATING) {
    // state that eating
    state[phnum] = EATING;

    printf("Philosopher %d takes fork %d and %d\n",
        phnum + 1, LEFT + 1, phnum + 1);
    printf("Philosopher %d is Eating\n", phnum + 1);
    eating(TIEMPO);
    ate[phnum]++;

    // used to wake up hungry philosophers
    // during putfork
    pthread_mutex_unlock(&S[phnum]);
  }
}

// take up chopsticks
void take_fork(int phnum)
{

  pthread_mutex_lock(&mutex);
  // state that hungry
  state[phnum] = HUNGRY;
  printf("Philosopher %d is Hungry\n", phnum + 1);
  // eat if neighbours are not eating
  test(phnum);
  pthread_mutex_unlock(&mutex);

  // if unable to eat wait to be signalled
  pthread_mutex_lock(&S[phnum]);
}

// put down chopsticks
void put_fork(int phnum)
{

  pthread_mutex_lock(&mutex);

  // state that thinking
  state[phnum] = THINKING;

  printf("Philosopher %d putting fork %d and %d down\n",
      phnum + 1, LEFT + 1, phnum + 1);
  printf("Philosopher %d is thinking\n", phnum + 1);
  test(LEFT);
  test(RIGHT);

  pthread_mutex_unlock(&mutex);
}

void* philosopher(void* num)
{
  int* i = num;
  while (1) {
    thinking(TIEMPO);
    take_fork(*i);
    eating(TIEMPO);
    put_fork(*i);
  }
}

int main()
{
  int i;
  pthread_t thread_id[N];
  int status;

  signal(SIGINT, controlc);
  srand(time(0));
  for (i = 0; i < N; i++) {
    phil[i] = i;
    ate[i] = 0;
    status = pthread_mutex_init(&S[i],NULL);
    assert(status == 0);
  }

  for (i = 0; i < N; i++) {
    pthread_create(&thread_id[i], NULL,philosopher,&phil[i]);
    printf("Philosopher %d is thinking\n", i + 1);
  }

  for (i = 0; i < N; i++)
    pthread_join(thread_id[i], NULL);
}
