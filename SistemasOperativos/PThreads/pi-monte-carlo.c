#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <math.h>

#define MAX_PUNTOS 800000000

double r2()
{
  return (double) rand() / (double)RAND_MAX;
}

int main(int argc, char* argv[])
{
  unsigned long puntos_in = 0, puntos_totales = 0, i;
  double tiempo;
  setlocale(LC_NUMERIC,"");
  printf("Total puntos a explorar: %lu\n", (unsigned long) MAX_PUNTOS);
  puntos_in = 0;
  puntos_totales = 0;
  for (i = 0; i < MAX_PUNTOS; i++)
  {
	double x;
	double y;
	x = r2();
	y = r2();
	if (sqrt(x * x + y * y) <= 1)
	  puntos_in++;
	puntos_totales++;
	if (i < 0) printf("Abortar ");
  }
  printf("Puntos in: %lu totales: %lu pi:%f\n", puntos_in, puntos_totales,4.0*((double)puntos_in/(double)puntos_totales));
}
