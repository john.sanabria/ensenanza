/**
- Tomado de 
    https://w3.cs.jmu.edu/kirkpams/OpenCSF/Books/csf/html/ThreadArgs.html

- Modificado por: John Sanabria - john.sanabria@correounivalle.edu.co
- Fecha: 2023-10-18

Compilar este programa arroja una ADVERTENCIA (WARNING) que no evita que la
compilación genere un binario.

--- NOTA TOMADA DEL SITIO WEB

Casting integral values to pointers and back again is a common practice for passing parameters to pthreads. However, while it is generally safe in practice, it is potentially a bug on some platforms. Specifically, this technique relies on the fact that pointers are at least as large as standard integer types. That is, int variables are typically (but not required to be) 32 bits in size. Modern CPU architectures tend to use 32- or 64-bit addresses. As such, casting a 32-bit int up to a void* then back to a 32-bit int is safe.

On the other hand, assume the argument was declared as a long variable instance. If the code is running on a 32-bit architecture (which is not uncommon for virtualized systems) but the long type is 64 bits in size, then half of the argument is lost by down-casting to the pointer for the call to pthread_create()!

---

*/

#include <assert.h>
#include <pthread.h>
#include <stdio.h>

#define MAX_THREADS 10

void *
child_thread (void *_args) {
  /* POTENTIALLY DANGEROUS TIMING */
  int arg = (int) _args;

  /* Print the local copy of the argument */
  printf ("Argument is %d\n", arg);
  pthread_exit (NULL);
}

int main() {
    pthread_t child[MAX_THREADS];
    int i;

    printf("*** Iniciando 'main' thread\n");
    for (i = 0; i < MAX_THREADS; i++) {
        assert(pthread_create(&child[i], NULL, child_thread, (void*) i) == 0);
    }
    for (i = 0; i < MAX_THREADS; i++) {
        pthread_join(child[i],NULL);
    }
    printf("+++ Finalizando 'main' thread\n");
    return 0;
}
