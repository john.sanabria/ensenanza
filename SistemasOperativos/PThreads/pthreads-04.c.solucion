/*
 * Este codigo muestra el posible desbalance que se puede presentar a la
 * hora de llevar a cabo el procesamiento de una tarea que es irregular en su
 * ejecución dependiendo de los datos de entrada a la tarea.
 *
 * Autor: John Sanabria - john.sanabria@correounivalle.edu.co
 * Fecha: 2023-05-17
 */

#include <stdio.h>
#include <pthread.h>

#define MAX_THREADS 4
#define MAX_FIBONACCI 44

// Declaracion de funciones
int fibonacci(int);
void* calcular_fibonacci(void*);
void imprimir_vector_fibonacci(int);

// Definicion de variable global a los hilos
int vector_fibonacci[MAX_FIBONACCI];

// ++++++
// Metodo MAIN
// ++++++
int main(int argc, char* argv[])
{
  int i, thread_id[MAX_THREADS];
  pthread_t ppid[MAX_THREADS];

  for (i = 0; i < MAX_FIBONACCI; i++)
	vector_fibonacci[i] = i;

  for (i = 0; i < MAX_THREADS; i++)
  {
	thread_id[i] = i;
	pthread_create(&ppid[i],NULL, calcular_fibonacci, (void*)&thread_id[i]);
  }

  for (i = 0; i < MAX_THREADS; i++)
  {
	pthread_join(ppid[i],NULL);
	imprimir_vector_fibonacci(i);
  }
}

//
// Implementacion de funciones declaradas anteriormente
//
int fibonacci(int n)
{
  if (n == 0 || n == 1)
	return n;
  return fibonacci(n - 1) + fibonacci(n - 2);
}

void* calcular_fibonacci(void *_id)
{
  int *id;
  int i;

  id = (int*)_id;
  for (i = *id; i < MAX_FIBONACCI; i+=MAX_THREADS)
	vector_fibonacci[i] = fibonacci(vector_fibonacci[i]);

  return NULL;
}

void imprimir_vector_fibonacci(int id)
{
  int i;

  printf("Imprimiendo datos del vector %d\n",id); fflush(stdout);
  for (i = id; i < MAX_FIBONACCI; i+=MAX_THREADS)
	printf("%d ", vector_fibonacci[i]);
  printf("\n");
}
