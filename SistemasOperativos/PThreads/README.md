# Introducción a Hilos en C

## Ejercicios

- **Después de revisar el código** `pthreads-03.c` &rarr; tome el código del
  archivo [`pthreads-03.c`](pthreads-03.c) y haga las modificaciones necesarias
  para que cada hilo haga su cálculo individual, cada hilo presente el valor
  calculado y sea el hilo de la función `main` quien tome esos valores parciales
  y los consolide.

- **Después de revisar el código** `pthreads-03.c` &rarr; modificar el código de
  modo que cada hilo guarde en su propio `counter` el valor calculado. 
  El hilo `main` se encargará de sumar los valores que cada hilo guardó en su
  propio `counter`.

- **Después de revisar el código** `pthread-04.c` &rarr; 

  - ¿Qué puede observar de esta ejecución? ¿Por qué podemos decir que está desequilibrada?
  - Al momento la manera como se lleva a cabo la ejecución es distribuir los primeros valores de la serie de Fibonacci a los primeros y así hasta llegar al final. Modifique el programa de modo que haya una distribución más uniforme de los datos a procesar, es decir, que el hilo 0 procese el dato 0, hilo 1 el dato 1, hilo 2 el dato 2, hilo 3 el dato 3, hilo 0 el dato 4, hilo 1 el dato 5 y así sucesivamente. 
  - Observe los tiempos de su nueva versión.

- **Después de revisar el código** `pthreads-06.c` &rarr; modifique el código
  [`pthread-03.c`](pthread-03.c) de modo que __cada hilo retorne su valor de
  `counter`__ y sea el hilo `main` el que sume los valores calculados por cada
  hilo.

