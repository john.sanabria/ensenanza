/**
- Tomado de 
    https://w3.cs.jmu.edu/kirkpams/OpenCSF/Books/csf/html/ThreadArgs.html

- Modificado por: John Sanabria - john.sanabria@correounivalle.edu.co
- Fecha: 2023-10-18

*/

#include <assert.h>
#include <pthread.h>
#include <stdio.h>

#define MAX_THREADS 10

void *
child_thread (void *args) {
  /* POTENTIALLY DANGEROUS TIMING */
  int *argptr = (int *) args;
  int arg = *argptr;

  /* Print the local copy of the argument */
  printf ("Argument is %d\n", arg);
  pthread_exit (NULL);
}

int main() {
    pthread_t child[MAX_THREADS];
    int i;

    printf("*** Iniciando 'main' thread\n");
    for (i = 0; i < MAX_THREADS; i++) {
        assert(pthread_create(&child[i],NULL,child_thread,&i) == 0);
    }
    for (i = 0; i < MAX_THREADS; i++) {
        pthread_join(child[i],NULL);
    }
    printf("+++ Finalizando 'main' thread\n");
    return 0;
}
