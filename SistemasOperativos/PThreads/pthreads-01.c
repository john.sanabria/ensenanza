/*
 * Este programa en C muestra el uso de la funcion 'pthread_create'
 *
 * Autor: John Sanabria - john.sanabria@correounivalle.edu.co
 * Fecha: 2023-05-17
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void *trabajo(void* arg)
{
  char *cadena;

  cadena = (char*) arg;

  printf("%s\n",cadena);

  return NULL;
}

int main(int argc, char* argv[])
{
  pthread_t ppid;
  char *cadena;

  if (argc == 2)
	cadena = argv[1];
  else
	cadena = "hola mundo";

  pthread_create(&ppid, NULL, trabajo, cadena);
  pthread_join(ppid,NULL);

  return 0;
}
