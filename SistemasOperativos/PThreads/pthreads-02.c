/*
 * Este programa en C muestra el problema potencial a la hora de pasar
 * variables a los hilos al momento de su creacion.
 *
 * En este caso el programa creara varios hilos y todos imprimiran una
 * cadena de caracteres.
 *
 * Autor: John Sanabria - john.sanabria@correounivalle.edu.co
 * Fecha: 2023-05-17
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define MAX_THREADS 4
#define MAX_CADENA 20

void *trabajo(void* arg)
{
  char *cadena;

  cadena = (char*) arg;

  printf("%s\n",cadena);

  return NULL;
}

int main(int argc, char* argv[])
{
  pthread_t ppid[MAX_THREADS];
  char *prefijo = "hilo",
       cadena[MAX_THREADS][MAX_CADENA];
  int i;

  for (i = 0; i < MAX_THREADS; i++)
  {
    sprintf(cadena[i],"%s-%d",prefijo,i);
    printf("Creando hilo %s\n",cadena[i]);
    pthread_create(&ppid[i], NULL, trabajo, cadena[i]);
  }

  for (i = 0; i < MAX_THREADS; i++)
    pthread_join(ppid[i],NULL);

  return 0;
}
