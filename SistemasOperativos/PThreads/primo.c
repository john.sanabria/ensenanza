#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>

#define TRUE 1
#define FALSE 0

#define LOW_LIMIT 1000000
#define HIGH_LIMIT 1050000

#define STRIDE 50000
#define MAXPRIMOS 14500

int esprimo(int);
int esprimoomp(int);
int fastprimo(int);

int main(int argc, char* argv[])
{
  int contador = 0;
  int iterador = 0;
  int *primo_lista;
  primo_lista = (int*)malloc(sizeof(int) * MAXPRIMOS);
  assert(primo_lista != NULL);
  for (int i = LOW_LIMIT; i <= HIGH_LIMIT; i = i+1)
  {
	if (esprimo(i))
	{
	  primo_lista[iterador++] = i;
	  contador++;
	}
  }
  for (int i = 0; i < contador; i++)
	printf("Primo %d --> %d\n",i,primo_lista[i]);

  return 0;
}

int esprimo(int n)
{
  int limit = (int)(n/2 + 1);
  int bandera;
  for (int i = 2 ; i <= limit; i++)
  {
	if (n % i == 0)
	  return 1;
  }
  return 0;
}
