#!/usr/bin/env bash
cont=0
script="./client.py"
if [ "${1}" == "" ]; then
  max=10
else
  max=${1}
fi
if [ "${2}" != "" ]; then
  script="${2}"
fi
command="${script} ${max}"
while [ 1 ]; do
 #./client.py &
 eval "${command} &" 
 cont=$(( cont + 1 ))
 echo "${cont} "
 if [ ${cont} -eq ${max} ]; then
  break;
 fi
done
