#!/usr/bin/env python3
#
# Tomado de: 
# https://stackoverflow.com/questions/23828264/how-to-make-a-simple-multithreaded-socket-server-in-python-that-remembers-client
#
# Author: John Sanabria - john.sanabria@correounivalle.edu.co
# Date: 2022-13-15
#
# 2022-03-15:
#   Se modifico el codigo original para que este servidor pudiera interoperar 
#   con el cliente cuyo codigo esta en este directorio en el archivo 
#   'client.py'
#
# 2023-02-27:
#   Se habilito el paso de argumentos
#

import socket
import threading
import getopt
import sys

def ayuda(comm):
    print("Modo de uso:")
    print(f"  {comm} [-h] [-p|--port <puerto>] [-b|--bs <buffer_size>]")

def gestionando_argumentos(argv):
    PORT = 9999
    BUF_SIZE = 1024

    opts, args = getopt.getopt(argv[1:],"hp:b:",["port=","bs="])
    for opt,arg in opts:
        if opt == '-h':
            ayuda(sys.argv[0])
            sys.exit(1)
        elif opt in ("-p","--port"):
            PORT = arg
        elif opt in ("-b","--bs"):
            BUF_SIZE = arg
    return (PORT, BUF_SIZE)


class ThreadedServer(object):
    def __init__(self, host, port, buf_size):
        self.host = host
        self.port = port
        self.buf_size = buf_size
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))

    def listen(self):
        self.sock.listen(5)
        cont = 0
        while True:
            client, address = self.sock.accept()
            cont = cont + 1
            threading.Thread(target = self.listenToClient,
                             args = (client,address, cont)).start()

    def listenToClient(self, client, address, cont): 
        #sending a TCP message to the client in the form of bytes 
        client.send(bytes('Hello! Welcome to Pythongeeks','utf-8')) 
        #getting the TCP message from the client and decoding it 
        c_msg=client.recv(self.buf_size).decode() 
        print(f'The message from the client {client} is {c_msg}')
        #This stops the connection
        client.close() 

if __name__ == "__main__":
    port_num, buf_size = gestionando_argumentos(sys.argv)
    port_num = int(port_num)
    buf_size = int(buf_size)
    print(f"Servidor escuchando por puert {port_num}, buffer size {buf_size}")
    ThreadedServer('',port_num,buf_size).listen()
