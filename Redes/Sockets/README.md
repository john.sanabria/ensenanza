# Ejemplos básicos de Cliente/Servidor en Python

En este directorio encontrará ejemplos básicos del uso de Sockets en Python para crear aplicaciones Cliente/Servidor.

[client.py](client.py) Código en Python de un cliente que recibe del servidor un mensaje. Para más detalles de como invocar el cliente ejecutar `./client.py -h`

[server.py](server.py) Código en Python de un servidor que está en espera de conexiones y al recibir una, envía un mensaje al **proceso** que lo contactó.  Para más detalles de como invocar el servidor ejecutar `./server.py -h`

[multithread_server.py](multithread_server.py) Código en Python de un servidor con capacidad de atender múltiples clientes. Para más detalles de como invocar el servidor ejecutar `./multithread_server.py -h` 

[to_run_client.sh](to_run_client.sh) Script en Bash para correr múltiples clientes que se conecten con el servidor.

## Ejemplo de una interacción entre cliente-servidor

Para llevar a cabo la interacción de un cliente con un servidor se deben ejecutar los siguientes comandos:

1. El servidor `python3 server.py -p 2345 `
2. El cliente `python3 client.py -p 2345 `

## Docker

Los servidores que se describieron aquí se pueden distribuir como imágenes de contenedor de Docker.
Para crear una imagen de contenedor llevar a cabo la ejecución del siguiente comando:

```
docker image build -t python-server .
```

La imagen de contenedor recien creada (`python-server`) tiene todos los archivos de este repositorio en el directorio `/work` dentro del contenedor.
Así mismo, este contenedor expone por defecto el puerto `2345` del contenedor para que se acceda al servidor dentro del contenedor.

La forma de ejecutar el servidor dentro del contenedor puede ser:

* **Sin argumentos** La siguiente línea ejecuta el contenedor en modo *daemon* o *dettached* (`-d`), se asocia el puerto `2345` del contenedorcon el puerto `2346` del anfitrión, los mensajes del servidor se imprimirán por pantalla (`-t`) y una vez terminada la ejecución del contenedor este se borra (`--rm`). 
Para ver los mensajes del servidor se debe ejecutar el comando `docker container logs <id>`, donde `<id>` es el identificador del contenedor.

```
docker container run -p 2346:2345 -t -d --rm python-server
```

* **Con argumentos** es posible invocar la ejecución de un servidor como si este se ejecutara desde la línea comandos

```
docker container run -p 2346:2345 -t -d --rm python-server server.py -p 2345
```

También es posible llevar a cabo la ejecución del servidor `multihread_server.py` de la siguiente manera:

* **Sin contenedores**

```
python3 multithread_server -p 2345
```

* **Con contenedores**

```
docker container run -p 2346:2345 -t -d --rm python-server multithread_server -p 2345
```

## Docker Compose

Para llevar a cabo la ejecución del servidor con `docker compose` se debe ejecutar el siguiente comando:

```
PORT=2345 docker compose up 
```

Una vez en ejecución se puede invocar el cliente de la forma usual `python3 client.py -p 2345`.

Para detener la ejecución se lleva a cabo la ejecución de la secuencia de teclas `ctrl-c`.
