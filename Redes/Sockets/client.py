#!/usr/bin/env python3
#
# Este codigo fue tomado de: https://pythongeeks.org/networking-in-python/
# y modificado por John Sanabria.
#
# Author: John Sanabria - john.sanabria@correounivalle.edu.co
# Date: 2022-03-15
#
# 2023-02-27:
#   Se modifico de modo que se le puedan pasar argumentos a la hora de lanzar
#   la ejecucion del programa. Argumentos tales como: puerto, tamano de datos
#   a recibir.
#
#importing the module
import socket
import sys
import getopt

def ayuda(comm):
    print("Modo de uso:")
    print(f"  {comm} [-h] [-s|--server <servidor>] [-p|--port <puerto>] [-b|--bs <buffer_size>]")

def gestionando_argumentos(argv):
    PORT = 9999
    BUF_SIZE = 1024
    HOSTNAME = socket.gethostname()

    opts, args = getopt.getopt(argv[1:],"hp:b:s:",["port=","bs=","server="])
    for opt,arg in opts:
        if opt == '-h':
            ayuda(sys.argv[0])
            sys.exit(1)
        elif opt in ("-p","--port"):
            PORT = arg
        elif opt in ("-b","--bs"):
            BUF_SIZE = arg
        elif opt in ("-s","--server"):
            HOSTNAME = arg
    return (PORT, BUF_SIZE, HOSTNAME)

if __name__ == '__main__':
    PORT, BUF_SIZE, HOSTNAME = gestionando_argumentos(sys.argv)
    PORT = int(PORT)
    BUF_SIZE = int(BUF_SIZE)
    print(f"Conectando al servidor {HOSTNAME} por el puerto {PORT}, tamano buffer {BUF_SIZE}")
    # Creating the socket
    cSct=socket.socket()
    # Connecting to the server
    cSct.connect((HOSTNAME,PORT))
    # Receiving the TCP message and printing it. Since the message is sent in
    # bytes, we have to decode it before printing
    print("The message from the server is ",cSct.recv(BUF_SIZE).decode())
    # Sending the TCP message to the server in bytes form
    cSct.send(bytes('Happy to connect with you!','utf-8'))
    # This closes the connection
    cSct.close()
