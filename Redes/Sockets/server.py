#!/usr/bin/env python3
#
# Este codigo fue tomado de: https://pythongeeks.org/networking-in-python/
#
# Este es un servidor un solo hilo y no acepta acceso concurrente.
#
# Author: John Sanabria - john.sanabria@correounivalle.edu.co
# Date: 2022-03-15
#
# 2022-03-15:
#   Se hicieron los siguientes ajustes en el codigo:
#   - To change ‘c.send’ by ‘client.send’
#   - To change ‘sct.recv’ by ‘client.recv’
#   - To change ‘sct.close’ by ‘client.close’
#
# 2023-02-27:
#   Se modifico de modo que se le puedan pasar argumentos a la hora de lanzar
#   la ejecucion del programa. Argumentos tales como: puerto, tamano de datos
#   a recibir.
# importing the module
import socket
import getopt
import sys

def ayuda(comm):
    print("Modo de uso:")
    print(f"  {comm} [-h] [-p|--port <puerto>] [-b|--bs <buffer_size>]")

def gestionando_argumentos(argv):
    PORT = 9999
    BUF_SIZE = 1024

    opts, args = getopt.getopt(argv[1:],"hp:b:",["port=","bs="])
    for opt,arg in opts:
        if opt == '-h':
            ayuda(sys.argv[0])
            sys.exit(1)
        elif opt in ("-p","--port"):
            PORT = arg
        elif opt in ("-b","--bs"):
            BUF_SIZE = arg
    return (PORT, BUF_SIZE)


if __name__ == "__main__":
    (PORT, BUF_SIZE) = gestionando_argumentos(sys.argv)
    PORT = int(PORT)
    # creating the socket 
    print(f"Creando socket por el puerto {PORT}, buffer size {BUF_SIZE}")
    sct=socket.socket()
    # binding the IP address and the port number by passing them as elements 
    # of a tuple 
    sct.bind((socket.gethostname(),PORT))
    # This listens and waits for 5 connections. We should give any limit, here 5, 
    # as we cannot accept infinite connections
    sct.listen(5)
    print('Waiting for connection...')
    # Now for every connection we want to interact. So we will write a loop
    cont=0
    while True:
        # this function accepts the connection from client and returns 
        # the client name and its address
        client,add=sct.accept()
        cont = cont + 1
        # sending a TCP message to the client in the form of bytes 
        client.send(bytes('Hello! Welcome to Pythongeeks','utf-8'))
        # getting the TCP message from the client and decoding it 
        c_msg=client.recv(BUF_SIZE).decode()
        # printing the message
        print(f'[{cont}] The message from the client {client} is {c_msg}')
        # This stops the connection
        client.close() 
